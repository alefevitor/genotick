package com.alphatica.genotick.account;

import com.alphatica.genotick.data.DataSetName;
import com.alphatica.genotick.genotick.Prediction;
import com.alphatica.genotick.ui.UserOutput;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public class Account {

    private Map<DataSetName, Prediction> pendingOrders = new HashMap<>();
    private Map<DataSetName, Trade> trades = new HashMap<>();
    private Map<DataSetName, BigDecimal> profits = new HashMap<>();

    private BigDecimal cash;
    private final UserOutput output;

    public Account(BigDecimal cash, UserOutput output) {
        this.cash = cash;
        this.output = output;
        output.reportAccountOpening(cash);
    }

    public BigDecimal getValue() {
        return cash.add(trades.values().stream().map(Trade::value).reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public void openTrades(Map<DataSetName, Double> prices) {
        if(!pendingOrders.isEmpty()) {
            BigDecimal cashPerTrade = cash.divide(BigDecimal.valueOf(pendingOrders.size()), MathContext.DECIMAL128);
            prices.forEach((name, price) -> openTrade(cashPerTrade, name, price));
        }
    }

    public void closeTrades(Map<DataSetName, Double> prices) {
        prices.forEach((name, price) -> closeTrade(name, BigDecimal.valueOf(price)));
    }

    public BigDecimal closeAccount() {
        List<DataSetName> openedTradesNames = new ArrayList<>(trades.keySet());
        openedTradesNames.forEach(name -> closeTrade(name, trades.get(name).getPrice()));
        reportMarketProfits();
        output.reportAccountClosing(cash);
        return cash;
    }

    private void reportMarketProfits() {
        profits.forEach((name, profit) -> {
            if(!name.isReverse()) {
                String reportedProfit = profit.subtract(BigDecimal.ONE).multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.DOWN).toPlainString();
                output.infoMessage(format("Total profit for market %s: %s %%", name.getName(), reportedProfit));
            }
        });
    }

    public void addPendingOrder(DataSetName name, Prediction prediction) {
        validateAddPending(name);
        if(prediction != Prediction.OUT) {
            output.reportPendingTrade(name, prediction);
            pendingOrders.put(name, prediction);
        }
    }

    BigDecimal getCash() {
        return cash;
    }

    private void openTrade(BigDecimal cashPerTrade, DataSetName name, Double price) {
        validateOpenTrade(name);
        ofNullable(pendingOrders.get(name)).ifPresent(prediction -> {
            BigDecimal quantity = cashPerTrade.divide(BigDecimal.valueOf(price), MathContext.DECIMAL128);
            if(prediction == Prediction.DOWN) {
                quantity = quantity.negate();
            }
            output.reportOpeningTrade(name, quantity, price);
            Trade trade = new Trade(quantity, BigDecimal.valueOf(price));
            trades.put(name, trade);
            pendingOrders.remove(name);
            cash = cash.subtract(cashPerTrade);
        });
    }

    private void closeTrade(DataSetName name, BigDecimal price) {
        ofNullable(trades.get(name)).ifPresent(trade -> {
            BigDecimal profit = trade.getQuantity().multiply(price.subtract(trade.getPrice()));
            BigDecimal initial = trade.getQuantity().abs().multiply(trade.getPrice());
            BigDecimal marketCurrentProfit = profits.computeIfAbsent(name, n -> BigDecimal.ONE);
            profits.put(name, marketCurrentProfit.multiply(profit.divide(initial, MathContext.DECIMAL32).add(BigDecimal.ONE)));
            cash = cash.add(profit).add(initial);
            output.reportClosingTrade(name, trade.getQuantity(), price, profit, cash);
            trades.remove(name);
        });
    }

    private void validateAddPending(DataSetName name) {
        if(pendingOrders.containsKey(name)) {
            throw new AccountException("results exist");
        }
    }

    private void validateOpenTrade(DataSetName name) {
        if(trades.containsKey(name)) {
            throw new AccountException("Previous trade not closed");
        }
    }
}
